/*
 * mqtt_client_utils.h
 *
 *  Created on: Feb 5, 2022
 *      Author: efi13
 */

#ifndef MQTT_CLIENT_UTILS_H_
#define MQTT_CLIENT_UTILS_H_



#include <stdio.h>
#include <string.h>
#include <time.h>
#include <inttypes.h>
#include "cellular.h"
#include "MQTTClient.h"
#include "global.h"

#define READ_BUFF_SIZE 512
#define WRITE_BUFF_SIZE 512
#define MSG_BUFF_SIZE 256
#define TOPIC_BUFF_SIZE 64

#define DEFAULT_MQTT_QOS 1
#define NUM_OF_OPERATORS 30
#define DEFAULT_KEEP_ALIVE_SEC 60
#define DEFAULT_CMD_TIMEOUT_MS  30000
#define DEFAULT_CON_TIMEOUT_MS  5000

static volatile word16 mPacketIdLast;

int format_ops_info(char* buf, int buf_size, OPERATOR_INFO* ops, int num_of_operators);

word16 mqtt_get_packetid(void);

void mqtt_init_ctx(MQTTCtx* mqttCtx);

void clean_network(MQTTCtx *mqttCtx);

int disconnect(MQTTCtx *mqttCtx);

int send_msg(MQTTCtx *mqttCtx);

void build_connect_packet(MQTTCtx *mqttCtx);

void set_lwt(MQTTCtx *mqttCtx, int active, char *lwt_msg_buf, char *lwt_topic);

int subscribe(MQTTCtx *mqttCtx);

int wait_for_message(MQTTCtx *mqttCtx);


#endif /* MQTT_CLIENT_UTILS_H_ */
