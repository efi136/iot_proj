#include "socket.h"
#include "cellular.h"
#include "global.h"
#include "gprintf.h"

#include <stdio.h>
#include <string.h>

#define COM_PORT "/dev/ttyS0"

#define CONN_INACTIVE_TIMEOUT 60
#define CONN_KEEPINTVL 40

#define NUM_OF_OPERATORS 30
#define ICCID_BUF_LEN 30
#define IMEI_BUF_LEN 30
#define IPv4_BUF_LEN 16
#define AUTO_OPT

#define FIND_COPS_ATTEMPTS 3

static char *g_host;
static int g_port;

/////// Public Functions ///////

int SocketInit(char *host, int port)
{
    char iccid[ICCID_BUF_LEN] = { 0 };
    char imei[IMEI_BUF_LEN] = { 0 };

    if (CellularInit(COM_PORT) < 0)
    {
        return FAILURE_CODE;
    }

    if (CellularWaitUntilModemResponds() < 0)
    {
        return FAILURE_CODE;
    }

    if (CellularGetIMEI(imei, IMEI_BUF_LEN) < 0)
    {
        return FAILURE_CODE;
    }

    gprintf("Socket: IMEI: %s\n", imei);

    if (CellularGetICCID(iccid, ICCID_BUF_LEN) < 0)
    {
        return FAILURE_CODE;
    }

    gprintf("Socket: ICCID: %s\n", iccid);

    if (CellularWaitUntilRegistered() < 0)
    {
        return FAILURE_CODE;
    }

    g_host = host;
    g_port = port;

    return SUCCESS_CODE;
}

int SocketConnect(void)
{
    int i, ops_found, signal;
    OPERATOR_INFO ops[NUM_OF_OPERATORS] = { 0 };
    char ipv4[IPv4_BUF_LEN] = { 0 };
    char ops_available = FALSE;
    char selectOpt = FALSE;

#ifndef AUTO_OPT
    for (i = 0; i < FIND_COPS_ATTEMPTS; i++)
    {
        if (CellularGetOperators(ops, NUM_OF_OPERATORS, &ops_found) < 0)
        {
            if (CellularWaitUntilModemResponds() < 0)
            {
                return FAILURE_CODE;
            }
        }
        else
        {
            ops_available = TRUE;
            break;
        }
    }

    if (!ops_available)
    {
        return FAILURE_CODE;
    }

    gprintf("Socket: ops found: %d\n", ops_found);
    for (i = 0; i < ops_found; i++)
    {
        gprintf("Socket: op name: %s\n", ops[i].operator_name);
        gprintf("Socket: operator code: %d\n", ops[i].operator_code);
        gprintf("Socket: access tech: %s\n", ops[i].access_technology);
        gprintf("Socket: op status: %d\n", ops[i].operator_status);
    }

    for (i = 0; i < ops_found; i++)
    {
        gprintf("Socket: registering to op %s with code %d\n", ops[i].operator_name, ops[i].operator_code);

        if (ops[i].operator_status != CURRENT_OPERATOR && ops[i].operator_status != OPERATOR_AVAILABLE)
        {
            gprintf("op is not available, skipping\n");
            continue;
        }

        if (CellularSetOperator(SET_OPT_MODE_MANUAL, ops[i].operator_code) < 0)
        {
            gprintf("Socket: Failed selecting op\n");
            continue;
        }

        gprintf("Socket: op selected\n");
        selectOpt = TRUE;
        break;
    }

    if (!selectOpt)
    {
        gprintf("Socket: No opt selected\n");
        return FAILURE_CODE;
    }
#else
#ifndef SKIP_OPT
    if (CellularSetOperator(SET_OPT_MODE_AUTO, 0) < 0)
    {
        gprintf("Socket: Failed registering\n");
        return FAILURE_CODE;
    }
#endif
#endif

    gprintf("Socket: selected operator successfully\n");

    if (CellularGetSignalQuality(&signal) < 0)
    {
        return FAILURE_CODE;
    }

    gprintf("Socket: signal is: %d\n", signal);

    if (CellularSetupInternetConnectionProfile(CONN_INACTIVE_TIMEOUT) < 0)
    {
        gprintf("Socket: Failed setting internet connection profile\n");
        return FAILURE_CODE;
    }

    if (CellularSetupInternetServiceProfile(g_host, g_port, CONN_KEEPINTVL) < 0)
    {
        gprintf("Socket: Failed setting internet service profile\n");
        return FAILURE_CODE;
    }

    if (CellularConnect() < 0)
    {
        return FAILURE_CODE;
    }

    return SUCCESS_CODE;
}

int SocketWrite(unsigned char *payload, unsigned int len)
{
    return CellularWrite(payload, len);
}

int SocketRead(unsigned char *buf, unsigned int max_len, unsigned int timeout_ms)
{
    return CellularRead(buf, max_len, timeout_ms);
}

int SocketClose(void)
{
    return CellularClose();
}

void SocketDeInit(void)
{
    CellularDisable();
}
