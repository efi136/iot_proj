#include "serial_io.h"

#include "sl_iostream.h"
#include "sl_iostream_init_instances.h"
#include "sl_iostream_handles.h"
#include "sl_iostream_usart_vcom_config.h"
#include "sl_sleeptimer.h"

#define UINT_MAX 65535

/**
 * @brief Initialises the serial connection.
 * @param port - the port to connected to. e.g: /dev/ttyUSB0, /dev/ttyS1 for Linux.
 * @param baud - the baud rate of the communication. For example: 9600, 115200
 * @return 0 if succeeded in opening the port and -1 otherwise.
 */
int SerialInit(char* port, unsigned int baud)
{
    (void)&port;

    // assumes sl_iostream_usart_vcom_config was used and
    // initialized in sl_system_init in the main
    if (baud != SL_IOSTREAM_USART_VCOM_BAUDRATE)
    {
        return -1;
    }

    // setting it to true should be OK when text is sent
    // but it shouldn't be used for binary data
    if (SL_IOSTREAM_USART_VCOM_CONVERT_BY_DEFAULT_LF_TO_CRLF != 0)
    {
        return -1;
    }

    sl_iostream_set_default(sl_iostream_vcom_handle);

    return 0;
}


int SerialRecvHelper(unsigned char *buf, unsigned int max_len, unsigned int timeout_ms){
  sl_status_t status;
    size_t bytes_read = 0;
    unsigned int left_timeout_ms;

    left_timeout_ms = timeout_ms;
    while (left_timeout_ms > 0)
    {
        if (left_timeout_ms > UINT_MAX)
        {
            sl_sleeptimer_delay_millisecond(UINT_MAX);
            left_timeout_ms -= UINT_MAX;
        }
        else
        {
            sl_sleeptimer_delay_millisecond(left_timeout_ms);
            left_timeout_ms = 0;
        }
    }

    status = sl_iostream_read(SL_IOSTREAM_STDIN, buf, max_len, &bytes_read);
    if (status == SL_STATUS_EMPTY)
    {
      return 0;
    }
    if (status != SL_STATUS_OK)
    {
      return -1;
    }

    return bytes_read;
}

/**
 * @brief Receives data from serial connection.
 * @param buf - the buffer that receives the input.
 * @param max_len - maximum bytes to read into buf (buf must be equal or greater than max_len).
 * @param timeout_ms - read operation timeout milliseconds.
 * @return amount of bytes read into buf, -1 on error.
*/
int SerialRecv(unsigned char *buf, unsigned int max_len, unsigned int timeout_ms)
{
    int bytes_read = 0, cur_bytes = 0;
    while (bytes_read < max_len){
        cur_bytes = SerialRecvHelper(buf + bytes_read, max_len - bytes_read, timeout_ms / 2);
        if (cur_bytes == -1){
            return -1;
        }
        if (cur_bytes == 0){
            return bytes_read;
        }
        bytes_read += cur_bytes;
    }
 }

/**
 * @brief Sends data through the serial connection.
 * @param buf - the buffer that contains the data to send
 * @param size - number of bytes to send
 * @return amount of bytes written into buf, -1 on error
 */
int SerialSend(unsigned char *buf, unsigned int size)
{
  if (sl_iostream_write(SL_IOSTREAM_STDOUT, buf, size) != SL_STATUS_OK)
  {
      return -1;
  }

  return size;
}

/**
 * @brief Empties the input buffer.
 */
void
SerialFlushInputBuff(void)
{
  sl_status_t status;
  char c;

  do
  {
      status = sl_iostream_getchar(SL_IOSTREAM_STDIN, &c);
  } while(status == SL_STATUS_OK && c > 0);
}

/**
 * @brief Disable the serial connection.
 * @return 0 if succeeded in closing the port and -1 otherwise.
 */
int SerialDisable(void)
{
  SerialFlushInputBuff();
  return 0;
}
