#include <stdio.h>
#include <string.h>
#include <time.h>
#include <inttypes.h>
#include "cellular.h"
#include "MQTTClient.h"
#include "global.h"
#include "gprintf.h"
#include "mqtt_client_utils.h"



#define READ_BUFF_SIZE 512
#define WRITE_BUFF_SIZE 512
#define MSG_BUFF_SIZE 256
#define TOPIC_BUFF_SIZE 64

#define DEFAULT_MQTT_QOS 1
#define NUM_OF_OPERATORS 30
#define DEFAULT_KEEP_ALIVE_SEC 60
#define DEFAULT_CMD_TIMEOUT_MS  30000
#define DEFAULT_CON_TIMEOUT_MS  5000

static char default_mqtt_host[] = "18.193.126.219";
static char default_mqtt_topic[] = "huji_iot_class/2021_2022";
static char default_mqtt_client_id[] = "efi";
static char second_mqtt_message_format[] = "{\"AvailableOperators\":[%s]}";
static char operator_format[] = "{\"OperatorName\":\"%s\",\"OperatorCode\":\"%d\",\"AccessTechnology\":\"%s\"},";


static volatile word16 mPacketIdLast;

int format_ops_info(char* buf, int buf_size, OPERATOR_INFO* ops, int num_of_operators) {
    int i = 0;
    int offset = 0;
    int cur_len = 0;
    char operators_buf[MSG_BUFF_SIZE];
    char single_operator_buf[MSG_BUFF_SIZE];
    memset(operators_buf, 0, MSG_BUFF_SIZE);
    memset(buf, 0, buf_size);
    for (i = 0; i < num_of_operators; i++) {
        memset(single_operator_buf, 0, MSG_BUFF_SIZE);
        sgprintf(single_operator_buf, operator_format, ops[i].operator_name, ops[i].operator_code, ops[i].access_technology);
        cur_len = strlen(single_operator_buf);
        memcpy(operators_buf + offset, single_operator_buf, cur_len);
        offset = offset + cur_len;
    }
    sgprintf(buf, second_mqtt_message_format, operators_buf);
    return 0;
}

word16 mqtt_get_packetid(void)
{
    /* Check rollover */
    if (mPacketIdLast >= MAX_PACKET_ID) {
        mPacketIdLast = 0;
    }

    return ++mPacketIdLast;
}

void mqtt_init_ctx(MQTTCtx* mqttCtx)
{
    XMEMSET(mqttCtx, 0, sizeof(MQTTCtx));
    mqttCtx->host = default_mqtt_host;
    mqttCtx->qos = DEFAULT_MQTT_QOS;
    mqttCtx->clean_session = 0;
    mqttCtx->keep_alive_sec = DEFAULT_KEEP_ALIVE_SEC;
    mqttCtx->client_id = default_mqtt_client_id;
    mqttCtx->topic_name = default_mqtt_topic;
    mqttCtx->cmd_timeout_ms = DEFAULT_CMD_TIMEOUT_MS;
}

void clean_network(MQTTCtx *mqttCtx)
{
    int rc;

    /* Cleanup network */
    rc = MqttClientNet_DeInit(&mqttCtx->net);
    if (rc != MQTT_CODE_SUCCESS) {
        gprintf("Main: Failed cleaning network resources\n");
        return;
    }

    MqttClient_DeInit(&mqttCtx->client);
}

int disconnect(MQTTCtx *mqttCtx)
{
    int rc;

    /* Disconnect */
    rc = MqttClient_Disconnect_ex(&mqttCtx->client, &mqttCtx->disconnect);

    gprintf("MQTT Disconnect: %s (%d)", MqttClient_ReturnCodeToString(rc), rc);
    if (rc != MQTT_CODE_SUCCESS) {
        gprintf("Main: failed disconnecting from MQTT broker\r");
        return rc;
    }

    rc = MqttClient_NetDisconnect(&mqttCtx->client);

    gprintf("MQTT Socket Disconnect: %s (%d)", MqttClient_ReturnCodeToString(rc), rc);

    return rc;
}

int send_msg(MQTTCtx *mqttCtx)
{
    int rc;

    /* Publish Topic */
    XMEMSET(&mqttCtx->publish, 0, sizeof(MqttPublish));
    mqttCtx->publish.retain = 0;
    mqttCtx->publish.qos = mqttCtx->qos;
    mqttCtx->publish.duplicate = 0;
    mqttCtx->publish.topic_name = mqttCtx->topic_name;
    mqttCtx->publish.packet_id = mqtt_get_packetid();

    mqttCtx->publish.buffer = (byte*)mqttCtx->message;
    mqttCtx->publish.total_len = (word16)XSTRLEN(mqttCtx->message);

    /* This loop allows payloads larger than the buffer to be sent by
       repeatedly calling publish.
    */
    do {
        rc = MqttClient_Publish(&mqttCtx->client, &mqttCtx->publish);
    } while(rc == MQTT_CODE_PUB_CONTINUE);


    gprintf("MQTT Publish: Topic %s, %s (%d)",
           mqttCtx->publish.topic_name,
           MqttClient_ReturnCodeToString(rc), rc);

    if (rc != MQTT_CODE_SUCCESS) {
        gprintf("Main: Failed publishing message to the broker\r");
        return FAILURE_CODE;
    }

    gprintf("Main: Successfully published message to the broker\r");
    return rc;
}

void build_connect_packet(MQTTCtx *mqttCtx)
{
    /* Build connect packet */
    XMEMSET(&mqttCtx->connect, 0, sizeof(MqttConnect));
    mqttCtx->connect.keep_alive_sec = mqttCtx->keep_alive_sec;
    mqttCtx->connect.clean_session = mqttCtx->clean_session;
    mqttCtx->connect.client_id = mqttCtx->client_id;
}

void set_lwt(MQTTCtx *mqttCtx, int active, char *lwt_msg_buf, char *lwt_topic)
{
    /* Last will and testament sent by broker to subscribers
    of topic when broker connection is lost */
    XMEMSET(&mqttCtx->lwt_msg, 0, sizeof(mqttCtx->lwt_msg));
    mqttCtx->enable_lwt = active;
    mqttCtx->connect.lwt_msg = &mqttCtx->lwt_msg;
    mqttCtx->connect.enable_lwt = mqttCtx->enable_lwt;

    if (mqttCtx->enable_lwt) {
        /* Send client id in LWT payload */
        mqttCtx->lwt_msg.qos = mqttCtx->qos;
        mqttCtx->lwt_msg.retain = FALSE;
        mqttCtx->lwt_msg.topic_name = lwt_topic;
        mqttCtx->lwt_msg.buffer = (byte *) lwt_msg_buf;
        mqttCtx->lwt_msg.total_len = (word16) XSTRLEN(lwt_msg_buf);
    }
}

int subscribe(MQTTCtx *mqttCtx)
{
    int rc, i;

    /* Build list of topics */
    XMEMSET(&mqttCtx->subscribe, 0, sizeof(MqttSubscribe));

    i = 0;
    mqttCtx->topics[i].topic_filter = mqttCtx->topic_name;
    mqttCtx->topics[i].qos = mqttCtx->qos;

    /* Subscribe Topic */
    mqttCtx->subscribe.packet_id = mqtt_get_packetid();
    mqttCtx->subscribe.topic_count =
            sizeof(mqttCtx->topics) / sizeof(MqttTopic);
    mqttCtx->subscribe.topics = mqttCtx->topics;

    rc = MqttClient_Subscribe(&mqttCtx->client, &mqttCtx->subscribe);

    gprintf("MQTT Subscribe: %s (%d)",
           MqttClient_ReturnCodeToString(rc), rc);
    if (rc != MQTT_CODE_SUCCESS) {
        return rc;
    }

    /* show subscribe results */
    for (i = 0; i < mqttCtx->subscribe.topic_count; i++) {
        MqttTopic *topic = &mqttCtx->subscribe.topics[i];
        gprintf("  Topic %s, Qos %u, Return Code %u",
               topic->topic_filter,
               topic->qos, topic->return_code);
    }

    return rc;
}

int wait_for_message(MQTTCtx *mqttCtx)
{
    int rc;

    /* Read Loop */
    gprintf("MQTT Waiting for message...");

    do {
        /* Try and read packet */
        rc = MqttClient_WaitMessage(&mqttCtx->client,
                                    mqttCtx->cmd_timeout_ms);


        /* check return code */
        if (rc == MQTT_CODE_ERROR_TIMEOUT) {
            /* Keep Alive */
            gprintf("Keep-alive timeout, sending ping");

            rc = MqttClient_Ping_ex(&mqttCtx->client, &mqttCtx->ping);
            if (rc != MQTT_CODE_SUCCESS) {
                gprintf("MQTT Ping Keep Alive Error: %s (%d)",
                       MqttClient_ReturnCodeToString(rc), rc);
                break;
            }
        }
        else if (rc != MQTT_CODE_SUCCESS) {
            /* There was an error */
            gprintf("MQTT Message Wait: %s (%d)",
                   MqttClient_ReturnCodeToString(rc), rc);
            break;
        }
        else if (rc == MQTT_CODE_SUCCESS) {
            gprintf("MQTT Message Wait: %s (%d)",
                   MqttClient_ReturnCodeToString(rc), rc);
            break;
        }
    } while (1);

    return rc;
}
