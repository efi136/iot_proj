#ifndef GECKO_SDK_3_2_3_GPRINTF_LCD_H_
#define GECKO_SDK_3_2_3_GPRINTF_LCD_H_

#include <stdarg.h>

void gprintf_init(void);
int gprintf(const char *format, ...);
int vgprintf(const char* format, va_list va);

#endif /* GECKO_SDK_3_2_3_GPRINTF_LCD_H_ */
