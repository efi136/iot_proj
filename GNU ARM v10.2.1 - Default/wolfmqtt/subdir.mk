################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../wolfmqtt/mqtt_client.c \
../wolfmqtt/mqtt_packet.c \
../wolfmqtt/mqtt_socket.c 

OBJS += \
./wolfmqtt/mqtt_client.o \
./wolfmqtt/mqtt_packet.o \
./wolfmqtt/mqtt_socket.o 

C_DEPS += \
./wolfmqtt/mqtt_client.d \
./wolfmqtt/mqtt_packet.d \
./wolfmqtt/mqtt_socket.d 


# Each subdirectory must supply rules for building sources it contributes
wolfmqtt/mqtt_client.o: ../wolfmqtt/mqtt_client.c
	@echo 'Building file: $<'
	@echo 'Invoking: GNU ARM C Compiler'
	arm-none-eabi-gcc -g3 -gdwarf-2 -mcpu=cortex-m4 -mthumb -std=c99 '-DDEBUG_EFM=1' '-DEFM32PG12B500F1024GL125=1' '-DSL_BOARD_NAME="BRD2501A"' '-DSL_BOARD_REV="A02"' '-DSL_COMPONENT_CATALOG_PRESENT=1' -I"C:\Users\efi13\SimplicityStudio\v5_workspace\iostream_usart_baremetal" -I"C:/Users/efi13/SimplicityStudio/SDKs/gecko_sdk//platform/Device/SiliconLabs/EFM32PG12B/Include" -I"C:/Users/efi13/SimplicityStudio/SDKs/gecko_sdk//platform/common/inc" -I"C:/Users/efi13/SimplicityStudio/SDKs/gecko_sdk//hardware/board/inc" -I"C:/Users/efi13/SimplicityStudio/SDKs/gecko_sdk//platform/driver/button/inc" -I"C:/Users/efi13/SimplicityStudio/SDKs/gecko_sdk//platform/CMSIS/Include" -I"C:/Users/efi13/SimplicityStudio/SDKs/gecko_sdk//platform/service/device_init/inc" -I"C:/Users/efi13/SimplicityStudio/SDKs/gecko_sdk//platform/emdrv/dmadrv/inc" -I"C:/Users/efi13/SimplicityStudio/SDKs/gecko_sdk//platform/middleware/glib/dmd" -I"C:/Users/efi13/SimplicityStudio/SDKs/gecko_sdk//platform/middleware/glib" -I"C:/Users/efi13/SimplicityStudio/SDKs/gecko_sdk//platform/emdrv/common/inc" -I"C:/Users/efi13/SimplicityStudio/SDKs/gecko_sdk//platform/emlib/inc" -I"C:/Users/efi13/SimplicityStudio/SDKs/gecko_sdk//platform/emlib/host/inc" -I"C:/Users/efi13/SimplicityStudio/SDKs/gecko_sdk//platform/middleware/glib/glib" -I"C:/Users/efi13/SimplicityStudio/SDKs/gecko_sdk//platform/emdrv/gpiointerrupt/inc" -I"C:/Users/efi13/SimplicityStudio/SDKs/gecko_sdk//platform/service/iostream/inc" -I"C:/Users/efi13/SimplicityStudio/SDKs/gecko_sdk//hardware/driver/memlcd/src/ls013b7dh03" -I"C:/Users/efi13/SimplicityStudio/SDKs/gecko_sdk//hardware/driver/memlcd/inc" -I"C:/Users/efi13/SimplicityStudio/SDKs/gecko_sdk//hardware/driver/memlcd/inc/memlcd_usart" -I"C:/Users/efi13/SimplicityStudio/SDKs/gecko_sdk//platform/common/toolchain/inc" -I"C:/Users/efi13/SimplicityStudio/SDKs/gecko_sdk//platform/service/system/inc" -I"C:/Users/efi13/SimplicityStudio/SDKs/gecko_sdk//platform/service/sleeptimer/inc" -I"C:/Users/efi13/SimplicityStudio/SDKs/gecko_sdk//platform/emdrv/uartdrv/inc" -I"C:/Users/efi13/SimplicityStudio/SDKs/gecko_sdk//platform/service/udelay/inc" -I"C:\Users\efi13\SimplicityStudio\v5_workspace\iostream_usart_baremetal\autogen" -I"C:\Users\efi13\SimplicityStudio\v5_workspace\iostream_usart_baremetal\config" -Os -Wall -Wextra -fno-builtin -ffunction-sections -fdata-sections -imacrossl_gcc_preinclude.h -mfpu=fpv4-sp-d16 -mfloat-abi=softfp -c -fmessage-length=0 -MMD -MP -MF"wolfmqtt/mqtt_client.d" -MT"wolfmqtt/mqtt_client.o" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

wolfmqtt/mqtt_packet.o: ../wolfmqtt/mqtt_packet.c
	@echo 'Building file: $<'
	@echo 'Invoking: GNU ARM C Compiler'
	arm-none-eabi-gcc -g3 -gdwarf-2 -mcpu=cortex-m4 -mthumb -std=c99 '-DDEBUG_EFM=1' '-DEFM32PG12B500F1024GL125=1' '-DSL_BOARD_NAME="BRD2501A"' '-DSL_BOARD_REV="A02"' '-DSL_COMPONENT_CATALOG_PRESENT=1' -I"C:\Users\efi13\SimplicityStudio\v5_workspace\iostream_usart_baremetal" -I"C:/Users/efi13/SimplicityStudio/SDKs/gecko_sdk//platform/Device/SiliconLabs/EFM32PG12B/Include" -I"C:/Users/efi13/SimplicityStudio/SDKs/gecko_sdk//platform/common/inc" -I"C:/Users/efi13/SimplicityStudio/SDKs/gecko_sdk//hardware/board/inc" -I"C:/Users/efi13/SimplicityStudio/SDKs/gecko_sdk//platform/driver/button/inc" -I"C:/Users/efi13/SimplicityStudio/SDKs/gecko_sdk//platform/CMSIS/Include" -I"C:/Users/efi13/SimplicityStudio/SDKs/gecko_sdk//platform/service/device_init/inc" -I"C:/Users/efi13/SimplicityStudio/SDKs/gecko_sdk//platform/emdrv/dmadrv/inc" -I"C:/Users/efi13/SimplicityStudio/SDKs/gecko_sdk//platform/middleware/glib/dmd" -I"C:/Users/efi13/SimplicityStudio/SDKs/gecko_sdk//platform/middleware/glib" -I"C:/Users/efi13/SimplicityStudio/SDKs/gecko_sdk//platform/emdrv/common/inc" -I"C:/Users/efi13/SimplicityStudio/SDKs/gecko_sdk//platform/emlib/inc" -I"C:/Users/efi13/SimplicityStudio/SDKs/gecko_sdk//platform/emlib/host/inc" -I"C:/Users/efi13/SimplicityStudio/SDKs/gecko_sdk//platform/middleware/glib/glib" -I"C:/Users/efi13/SimplicityStudio/SDKs/gecko_sdk//platform/emdrv/gpiointerrupt/inc" -I"C:/Users/efi13/SimplicityStudio/SDKs/gecko_sdk//platform/service/iostream/inc" -I"C:/Users/efi13/SimplicityStudio/SDKs/gecko_sdk//hardware/driver/memlcd/src/ls013b7dh03" -I"C:/Users/efi13/SimplicityStudio/SDKs/gecko_sdk//hardware/driver/memlcd/inc" -I"C:/Users/efi13/SimplicityStudio/SDKs/gecko_sdk//hardware/driver/memlcd/inc/memlcd_usart" -I"C:/Users/efi13/SimplicityStudio/SDKs/gecko_sdk//platform/common/toolchain/inc" -I"C:/Users/efi13/SimplicityStudio/SDKs/gecko_sdk//platform/service/system/inc" -I"C:/Users/efi13/SimplicityStudio/SDKs/gecko_sdk//platform/service/sleeptimer/inc" -I"C:/Users/efi13/SimplicityStudio/SDKs/gecko_sdk//platform/emdrv/uartdrv/inc" -I"C:/Users/efi13/SimplicityStudio/SDKs/gecko_sdk//platform/service/udelay/inc" -I"C:\Users\efi13\SimplicityStudio\v5_workspace\iostream_usart_baremetal\autogen" -I"C:\Users\efi13\SimplicityStudio\v5_workspace\iostream_usart_baremetal\config" -Os -Wall -Wextra -fno-builtin -ffunction-sections -fdata-sections -imacrossl_gcc_preinclude.h -mfpu=fpv4-sp-d16 -mfloat-abi=softfp -c -fmessage-length=0 -MMD -MP -MF"wolfmqtt/mqtt_packet.d" -MT"wolfmqtt/mqtt_packet.o" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

wolfmqtt/mqtt_socket.o: ../wolfmqtt/mqtt_socket.c
	@echo 'Building file: $<'
	@echo 'Invoking: GNU ARM C Compiler'
	arm-none-eabi-gcc -g3 -gdwarf-2 -mcpu=cortex-m4 -mthumb -std=c99 '-DDEBUG_EFM=1' '-DEFM32PG12B500F1024GL125=1' '-DSL_BOARD_NAME="BRD2501A"' '-DSL_BOARD_REV="A02"' '-DSL_COMPONENT_CATALOG_PRESENT=1' -I"C:\Users\efi13\SimplicityStudio\v5_workspace\iostream_usart_baremetal" -I"C:/Users/efi13/SimplicityStudio/SDKs/gecko_sdk//platform/Device/SiliconLabs/EFM32PG12B/Include" -I"C:/Users/efi13/SimplicityStudio/SDKs/gecko_sdk//platform/common/inc" -I"C:/Users/efi13/SimplicityStudio/SDKs/gecko_sdk//hardware/board/inc" -I"C:/Users/efi13/SimplicityStudio/SDKs/gecko_sdk//platform/driver/button/inc" -I"C:/Users/efi13/SimplicityStudio/SDKs/gecko_sdk//platform/CMSIS/Include" -I"C:/Users/efi13/SimplicityStudio/SDKs/gecko_sdk//platform/service/device_init/inc" -I"C:/Users/efi13/SimplicityStudio/SDKs/gecko_sdk//platform/emdrv/dmadrv/inc" -I"C:/Users/efi13/SimplicityStudio/SDKs/gecko_sdk//platform/middleware/glib/dmd" -I"C:/Users/efi13/SimplicityStudio/SDKs/gecko_sdk//platform/middleware/glib" -I"C:/Users/efi13/SimplicityStudio/SDKs/gecko_sdk//platform/emdrv/common/inc" -I"C:/Users/efi13/SimplicityStudio/SDKs/gecko_sdk//platform/emlib/inc" -I"C:/Users/efi13/SimplicityStudio/SDKs/gecko_sdk//platform/emlib/host/inc" -I"C:/Users/efi13/SimplicityStudio/SDKs/gecko_sdk//platform/middleware/glib/glib" -I"C:/Users/efi13/SimplicityStudio/SDKs/gecko_sdk//platform/emdrv/gpiointerrupt/inc" -I"C:/Users/efi13/SimplicityStudio/SDKs/gecko_sdk//platform/service/iostream/inc" -I"C:/Users/efi13/SimplicityStudio/SDKs/gecko_sdk//hardware/driver/memlcd/src/ls013b7dh03" -I"C:/Users/efi13/SimplicityStudio/SDKs/gecko_sdk//hardware/driver/memlcd/inc" -I"C:/Users/efi13/SimplicityStudio/SDKs/gecko_sdk//hardware/driver/memlcd/inc/memlcd_usart" -I"C:/Users/efi13/SimplicityStudio/SDKs/gecko_sdk//platform/common/toolchain/inc" -I"C:/Users/efi13/SimplicityStudio/SDKs/gecko_sdk//platform/service/system/inc" -I"C:/Users/efi13/SimplicityStudio/SDKs/gecko_sdk//platform/service/sleeptimer/inc" -I"C:/Users/efi13/SimplicityStudio/SDKs/gecko_sdk//platform/emdrv/uartdrv/inc" -I"C:/Users/efi13/SimplicityStudio/SDKs/gecko_sdk//platform/service/udelay/inc" -I"C:\Users\efi13\SimplicityStudio\v5_workspace\iostream_usart_baremetal\autogen" -I"C:\Users\efi13\SimplicityStudio\v5_workspace\iostream_usart_baremetal\config" -Os -Wall -Wextra -fno-builtin -ffunction-sections -fdata-sections -imacrossl_gcc_preinclude.h -mfpu=fpv4-sp-d16 -mfloat-abi=softfp -c -fmessage-length=0 -MMD -MP -MF"wolfmqtt/mqtt_socket.d" -MT"wolfmqtt/mqtt_socket.o" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


