/*
 * gprintf.c
 *
 *  Created on: Feb 5, 2022
 *      Author: efi13
 */


#include <stdio.h>
#include <stdarg.h>
#include "gprintf.h"
#include "string.h"
#include "sl_board_control.h"
#include "em_assert.h"
#include "glib.h"
#include "dmd.h"

#define BUF_SIZE  4096
#define SCREEN_BUF 21
#define MAX_LINE 12

static char buf[BUF_SIZE];
static char newline = '\n';

static GLIB_Context_t glibContext;
static int currentLine;

void gprintf_init(void)
{
  uint32_t status;

  /* Enable the memory lcd */
  status = sl_board_enable_display();
  EFM_ASSERT(status == SL_STATUS_OK);

  /* Initialize the DMD support for memory lcd display */
  status = DMD_init(0);
  EFM_ASSERT(status == DMD_OK);

  /* Initialize the glib context */
  status = GLIB_contextInit(&glibContext);
  EFM_ASSERT(status == GLIB_OK);

  glibContext.backgroundColor = White;
  glibContext.foregroundColor = Black;

  /* Fill lcd with background color */
  GLIB_clear(&glibContext);

  /* Use Narrow font */
  GLIB_setFont(&glibContext, (GLIB_Font_t *) &GLIB_FontNarrow6x8);
}

int gprintf(const char *format, ...)
{
  int ret;
  va_list va;

  va_start(va, format);
  ret = vgprintf(format, va);
  va_end(va);

  return ret;
}

int vgprintf(const char* format, va_list va)
{
  static char line[SCREEN_BUF];
  int size;
  char *buf_pos, *newline_pos;

  currentLine = 0;
  memset(buf, 0, BUF_SIZE);

  vsnprintf(buf, BUF_SIZE - 1, format, va);
  buf_pos = buf;

  size = strlen(buf);
  GLIB_clear(&glibContext);
  do
  {
      memset(line, 0, SCREEN_BUF);
      strncpy(line, buf_pos, SCREEN_BUF - 1);

      newline_pos = strchr(line, newline);
      if (newline_pos != NULL)
      {
          *(newline_pos + 1) = '\0';
      }
      buf_pos += strlen(line);


      GLIB_drawStringOnLine(&glibContext,
                            line,
                            currentLine,
                            GLIB_ALIGN_LEFT,
                            5,
                            5,
                            true);

      DMD_updateDisplay();

      currentLine++;
   } while(buf_pos - buf < size && currentLine < MAX_LINE);

  if (buf_pos - buf < size)
  {
      return buf_pos - buf;
  }
  return size;
}
