/***************************************************************************//**
 * @file
 * @brief iostream usart examples functions
 *******************************************************************************
 * # License
 * <b>Copyright 2020 Silicon Laboratories Inc. www.silabs.com</b>
 *******************************************************************************
 *
 * The licensor of this software is Silicon Laboratories Inc. Your use of this
 * software is governed by the terms of Silicon Labs Master Software License
 * Agreement (MSLA) available at
 * www.silabs.com/about-us/legal/master-software-license-agreement. This
 * software is distributed to you in Source Code format and is governed by the
 * sections of the MSLA applicable to Source Code.
 *
 ******************************************************************************/
#include <stdio.h>
#include <string.h>
#include "gprintf.h"
#include "em_chip.h"
#include "sl_iostream.h"
#include "sl_iostream_init_instances.h"
#include "sl_iostream_handles.h"
#include "sl_sleeptimer.h"
#include "sl_simple_button_instances.h"
#include "serial_io.h"
#include "cellular.h"
#include "global.h"
#include "MQTTClient.h"

/*******************************************************************************
 *******************************   DEFINES   ***********************************
 ******************************************************************************/

#define READ_BUFF_SIZE 512
#define WRITE_BUFF_SIZE 512
#define MSG_BUFF_SIZE 256
#define TOPIC_BUFF_SIZE 64

#define DEFAULT_MQTT_QOS 1
#define NUM_OF_OPERATORS 30
#define DEFAULT_KEEP_ALIVE_SEC 60
#define DEFAULT_CMD_TIMEOUT_MS  30000
#define DEFAULT_CON_TIMEOUT_MS  5000


#ifndef CLICKS_MSG_BUF_SIZE
#define CLICKS_MSG_BUF_SIZE    80
#endif

#ifndef RX_BUF_SIZE
#define RX_BUF_SIZE    100
#endif

#ifndef RX_MSG_BUF_SIZE
#define RX_MSG_BUF_SIZE    160
#endif

#ifndef BUTTON_INSTANCE_0
#define BUTTON_INSTANCE_0   sl_button_btn0
#endif

#ifndef BUTTON_INSTANCE_1
#define BUTTON_INSTANCE_1   sl_button_btn1
#endif

#ifndef PRINT_CLICKS_DELAY_MS
#define PRINT_CLICKS_DELAY_MS           10000
#endif

#ifndef COMM_BAUD
#define COMM_BAUD           115200
#endif


/*******************************************************************************
 ***************************  LOCAL VARIABLES   ********************************
 ******************************************************************************/


static char rx_buffer[RX_BUF_SIZE];
static char clicks_msg[CLICKS_MSG_BUF_SIZE];
static char rx_msg[RX_MSG_BUF_SIZE];
static char tx_msg_template[] = "number of clicks: %d\n";
static char rx_msg_template[] = "received: %s\n";
static bool cellular_init = false;


static char read_buffer[READ_BUFF_SIZE], write_buffer[WRITE_BUFF_SIZE];
static char msg[MSG_BUFF_SIZE], lwt_msg_buf[MSG_BUFF_SIZE];
static char second_msg_topic[TOPIC_BUFF_SIZE], lwt_topic[TOPIC_BUFF_SIZE], subscribe_id_topic[TOPIC_BUFF_SIZE];
static char default_mqtt_message[] = "{"
                                     "\"Student1ID\":\"208727693\","
                                     "\"Student2ID\":\"308514975\","
                                     "\"Student1Name\":\"Efi Sapir\","
                                     "\"Student2Name\":\"Menachem Adin\","
                                     "\"Identifier\":\"%s\""
                                     "}";
static char mqtt_unique_topic_id[TOPIC_BUFF_SIZE];

static char default_mqtt_topic[] = "huji_iot_class/2021_2022";
static char second_mqtt_topic_format[] = "huji_iot_class/2021_2022/%s";
static char mqtt_lwt_topic_suffix[] = "disconnect";


static char subscribe_topic[] = "huji_iot_class/2021_2022/%s/recv/#";

static char end_message[] = "{"
                            "\"DisconnectedGracefully\":%s"
                            "}";


static char mqtt_unique_topic_id[TOPIC_BUFF_SIZE];


int rc;
MQTTCtx mqttCtx;


/*******************************************************************************
 **************************   GLOBAL FUNCTIONS   *******************************
 ******************************************************************************/

static int mqtt_message_cb(MqttClient *client, MqttMessage *msg,
    byte msg_new, byte msg_done)
{
    byte buf[MSG_BUFF_SIZE+1];
    word32 len;
    MQTTCtx* mqttCtx = (MQTTCtx*)client->ctx;

    (void)mqttCtx;

    if (msg_new) {
        /* Determine min size to dump */
        len = msg->topic_name_len;
        if (len > MSG_BUFF_SIZE) {
            len = MSG_BUFF_SIZE;
        }
        XMEMCPY(buf, msg->topic_name, len);
        buf[len] = '\0'; /* Make sure its null terminated */

        /* Print incoming message */
        gprintf("MQTT Message: Topic %s, Qos %d, Len %u",
            buf, msg->qos, msg->total_len);
    }

    /* Print message payload */
    len = msg->buffer_len;
    if (len > MSG_BUFF_SIZE) {
        len = MSG_BUFF_SIZE;
    }
    XMEMCPY(buf, msg->buffer, len);
    buf[len] = '\0'; /* Make sure its null terminated */
    gprintf("Payload (%d - %d): %s",
        msg->buffer_pos, msg->buffer_pos + len, buf);

    if (msg_done) {
        gprintf("MQTT Message: Done");
    }

    /* Return negative to terminate publish processing */
    return MQTT_CODE_SUCCESS;
}


/***************************************************************************//**
 * Initialize example.
 ******************************************************************************/
void app_clicks_comm_init(void)
{
  gprintf_init();

  /* Prevent buffering of output/input.*/
#if !defined(__CROSSWORKS_ARM) && defined(__GNUC__)
  setvbuf(stdout, NULL, _IONBF, 0);   /*Set unbuffered mode for stdout (newlib)*/
  setvbuf(stdin, NULL, _IONBF, 0);   /*Set unbuffered mode for stdin (newlib)*/
#endif

  gprintf("Init: Starting program\n");

  /* init defaults */
  mqtt_init_ctx(&mqttCtx);
  mqttCtx.app_name = "mqttclient";

  bzero(msg, MSG_BUFF_SIZE);
  if (sprintf(msg, default_mqtt_message, mqtt_unique_topic_id) < 0)
  {
      gprintf("Init: Failed assembling message\r");
      return;
  }

  gprintf("MQTT Client: QoS %d, Use TLS %d", mqttCtx.qos, mqttCtx.use_tls);

  /* Initialize Network */
  rc = MqttClientNet_Init(&(mqttCtx.net), &mqttCtx);
  gprintf("MQTT Net Init: %s (%d)", MqttClient_ReturnCodeToString(rc), rc);
  if (rc != MQTT_CODE_SUCCESS) {
      clean_network(&mqttCtx);
      return;
  }

  /* setup tx/rx buffers */
  bzero(write_buffer, READ_BUFF_SIZE);
  bzero(read_buffer, WRITE_BUFF_SIZE);

  mqttCtx.tx_buf = write_buffer;
  mqttCtx.rx_buf = read_buffer;

  /* Initialize MqttClient structure */
  rc = MqttClient_Init(&mqttCtx.client, &mqttCtx.net,
      mqtt_message_cb,
      mqttCtx.tx_buf, WRITE_BUFF_SIZE,
      mqttCtx.rx_buf, READ_BUFF_SIZE,
      mqttCtx.cmd_timeout_ms);

  gprintf("MQTT Init: %s (%d)", MqttClient_ReturnCodeToString(rc), rc);
  if (rc != MQTT_CODE_SUCCESS) {
      clean_network(&mqttCtx);
      return;
  }

  /* The client.ctx will be stored in the cert callback ctx during
   * MqttSocket_Connect for use by mqtt_tls_verify_cb
   */
  mqttCtx.client.ctx = &mqttCtx;

  /* Connect to broker */
    rc = MqttClient_NetConnect(&mqttCtx.client, mqttCtx.host,
         mqttCtx.port,
      DEFAULT_CON_TIMEOUT_MS, mqttCtx.use_tls, NULL);

    gprintf("MQTT Socket Connect: %s (%d)", MqttClient_ReturnCodeToString(rc), rc);
    if (rc != MQTT_CODE_SUCCESS) {
      clean_network(&mqttCtx);
      return;
    }

    build_connect_packet(&mqttCtx);

    /* Last will and testament */
    bzero(lwt_msg_buf, MSG_BUFF_SIZE);
    if (sprintf(lwt_msg_buf, end_message, "false") < 0)
    {
      gprintf("Init: Failed assembling message\r");
      return;
    }

    bzero(lwt_topic, TOPIC_BUFF_SIZE);
    strcpy(lwt_topic, default_mqtt_topic);
    strcat(lwt_topic,  "/");
    strcat(lwt_topic, mqtt_lwt_topic_suffix);

    set_lwt(&mqttCtx, TRUE, lwt_msg_buf, lwt_topic);

    /* Optional authentication */
    mqttCtx.connect.username = mqttCtx.username;
    mqttCtx.connect.password = mqttCtx.password;

    rc = MqttClient_Connect(&mqttCtx.client, &mqttCtx.connect);

    gprintf("MQTT Connect: Proto (%s), %s (%d)",
      MqttClient_GetProtocolVersionString(&mqttCtx.client),
      MqttClient_ReturnCodeToString(rc), rc);
    if (rc != MQTT_CODE_SUCCESS) {
      gprintf("Init: Failed connecting to broker\r");
      disconnect(&mqttCtx);
      clean_network(&mqttCtx);
      return;
    }

    /* Validate Connect Ack info */
    gprintf("MQTT Connect Ack: Return Code %u, Session Present %d",
      mqttCtx.connect.ack.return_code,
      (mqttCtx.connect.ack.flags &
          MQTT_CONNECT_ACK_FLAG_SESSION_PRESENT) ?
          1 : 0
    );

    cellular_init = true;

}

void app_clicks_comm_process_action(void)
{
  int8_t c = 0;
  int recv_bytes = 0;

  if (!cellular_init)
  {
      return;
  }
/*
  if (CellularGetIMEI(mqtt_unique_topic_id, TOPIC_BUFF_SIZE) < 0)
  {
    gprintf("Main Failed getting IMEI\n");
    return;
  }

  bzero(subscribe_id_topic, TOPIC_BUFF_SIZE);
  if (sprintf(subscribe_id_topic, subscribe_topic, mqtt_unique_topic_id) < 0)
  {
    gprintf("Main: Failed assembling topic\r");
    return;
  }

  mqttCtx.topic_name = subscribe_id_topic;
  rc = subscribe(&mqttCtx);
  if (rc != MQTT_CODE_SUCCESS) {
    disconnect(&mqttCtx);
    clean_network(&mqttCtx);
    return;
  }

  // msg 1 - general topic
  mqttCtx.topic_name = default_mqtt_topic;
  mqttCtx.message = msg;
  if (send_msg(&mqttCtx) < 0)
  {
    disconnect(&mqttCtx);
    clean_network(&mqttCtx);
    return;
  }

  // msg 2 - specific topic

  OPERATOR_INFO ops[NUM_OF_OPERATORS] = { 0 };
  int num_of_ops_found = 0;
  if (CellularGetOperators(ops, NUM_OF_OPERATORS, &num_of_ops_found) < 0) {
    return;
  }

  if (format_ops_info(msg, MSG_BUFF_SIZE, ops, num_of_ops_found) < 0) {
    return;
  }

  bzero(msg, MSG_BUFF_SIZE);
  if (sprintf(msg, second_mqtt_topic_format, ops) < 0)
  {
    gprintf("Main: Failed assembling message\r");
    return;
  }

  bzero(second_msg_topic, TOPIC_BUFF_SIZE);
  strcpy(second_msg_topic, default_mqtt_topic);
  strcat(second_msg_topic,  "/");
  strcat(second_msg_topic, mqtt_unique_topic_id);

  mqttCtx.message = msg;
  mqttCtx.topic_name = second_msg_topic;
  if (send_msg(&mqttCtx) < 0)
  {
    disconnect(&mqttCtx);
    clean_network(&mqttCtx);
    return;
  }

  bzero(msg, MSG_BUFF_SIZE);
  if (sprintf(msg, end_message, "true") < 0)
  {
    gprintf("Main: Failed assembling message\r");
    return;
  }

  if (wait_for_message(&mqttCtx) < 0)
  {
    disconnect(&mqttCtx);
    clean_network(&mqttCtx);
    return;
  }

  // msg 3 - end message
  mqttCtx.message = msg;
  mqttCtx.topic_name = lwt_topic;
  mqttCtx.return_code = send_msg(&mqttCtx);

  rc = disconnect(&mqttCtx);

  if (rc != MQTT_CODE_SUCCESS) {
    gprintf("Main: failed disconnecting from socket\r");
  }

  clean_network(&mqttCtx);

  gprintf("Main: Ending program\n");

  return;

*/
}
